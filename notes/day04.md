# DQL - SELECT
* cmd> mysql -u ekdac -pekdac kdacdb

```SQL
SELECT USER(), DATABASE();
-- ekdac, kdacdb.

SHOW TABLES;

SELECT * FROM books;

SELECT * FROM emp;
```

## Select Rows/Columns

```SQL
SELECT * FROM books;
-- * means all columns.

SELECT id,name,price FROM books;

SELECT empno,ename,sal FROM emp;

SELECT empno AS 'emp id',ename AS 'emp name',sal AS 'salary' FROM emp;
-- AS is ANSI keyword, but optional.
-- alias names are quoted with '---' or `---`.
-- if no space or special char in alias name, then quotes are optional.

SELECT empno 'emp id',ename 'emp name',sal salary FROM emp;

-- fetch empno, ename, sal and da (e.g. sal - 50%) of the employee
DESCRIBE emp;
```

## Computed columns

```SQL

SELECT empno, ename, sal, sal * 0.5 FROM emp;

SELECT empno, ename, sal, sal * 0.5 AS da FROM emp;

SELECT empno, ename, sal, sal * 0.5 da FROM emp;

-- id, name, price, gst (e.g. 5%), total (price+gst) -- books
SELECT id, name, price FROM books;

SELECT id, name, price, price * 0.05 AS gst FROM books;

SELECT id, name, price, price * 0.05 AS gst, price + price * 0.05 AS total FROM books;

-- get empno, ename, sal and category (<= 1500 --> POOR, 1500 > and <= 2500 --> MIDDLE, 2500 > --> RICH)
SELECT empno, ename, sal FROM emp;

SELECT empno, ename, sal,
CASE
WHEN sal <= 1500 THEN 'Poor'
WHEN sal > 1500 AND sal <= 2500 THEN 'Middle'
ELSE 'Rich'
END AS category
FROM emp;

```

## DISTINCT

```SQL
-- fetch all unique subjects from books
SELECT DISTINCT subject FROM books;

-- fetch all unique deptno from emp
SELECT DISTINCT deptno FROM emp;

-- fetch all unique job from emp
SELECT DISTINCT job FROM emp;

-- fetch unique jobs per dept OR unique dept per job OR unique combinations of deptno & job.
SELECT DISTINCT deptno,job FROM emp;
```

## LIMIT clause
* SELECT cols FROM tablename LIMIT n;
	- n rows to fetch
* SELECT cols FROM tablename LIMIT m,n;
	- m rows to skip
	- n rows to fetch (after skipped rows)

```SQL
-- get first 5 books
SELECT * FROM books LIMIT 5;

-- get 2 books after skipping first 3 books
SELECT * FROM books LIMIT 3, 2;
```

## ORDER BY clause
* SELECT cols FROM tablename ORDER BY colname; --> asc sort by column
* SELECT cols FROM tablename ORDER BY colname ASC; --> asc sort by column -- ASC keyword is optional.
* SELECT cols FROM tablename ORDER BY colname DESC; --> desc sort by colname.

```SQL
SELECT * FROM books;

SELECT * FROM books ORDER BY price; 

SELECT * FROM books ORDER BY price DESC;

SELECT * FROM books ORDER BY author;

SELECT * FROM emp ORDER BY hire;

-- get all emp details sorted by their dept (asc)
SELECT * FROM emp ORDER BY deptno;

-- get job and depts in sorted order
SELECT deptno, job FROM emp ORDER BY deptno, job;
SELECT deptno, job FROM emp ORDER BY job, deptno;

-- get all emps sorted by dept (asc) and sal (desc)
SELECT * FROM emp ORDER BY deptno ASC, sal DESC;

```

## ORDER BY + LIMIT

```SQL
-- get emp with highest sal
SELECT * FROM emp ORDER BY sal DESC;
SELECT * FROM emp ORDER BY sal DESC LIMIT 1;

-- get emp with lowest sal
SELECT * FROM emp ORDER BY sal ASC;
SELECT * FROM emp ORDER BY sal ASC LIMIT 1;

-- get the "book" with third highest price.
SELECT * FROM books ORDER BY price DESC;

SELECT * FROM books ORDER BY price DESC LIMIT 2,1;
```

## WHERE clause
* SELECT cols FROM tablename WHERE condition;
* Relational operators
	* <, >, <=, >=, ==, != or <>
* Logical operators
	* AND, OR, NOT

```SQL
-- get all books with price > 500.
SELECT * FROM books WHERE price > 500;

-- get all emp having job = SALESMAN.
SELECT * FROM emp WHERE job = 'SALESMAN';

-- get all clerk in dept 20.
SELECT * FROM emp WHERE deptno = 20 AND job = 'CLERK';

```
