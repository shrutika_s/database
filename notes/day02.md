# MySQL root login
* cmd> mysql -u root -p
	* password: manager

```SQL
-- list all users of this mysql server
SELECT user FROM mysql.user;

-- list all databases on this mysql server
SHOW DATABASES;

EXIT;
```

# MySQL ekdac logic

* cmd> mysql -h localhost -u ekdac -p
	* -h: server ip address/name -- localhost (current computer)
		* default = localhost
	* -u: user name -- db user to login into mysql
	* -p: password -- while creating user -- IDENTIFIED BY 'ekdac'
		* password: ekdac

* cmd> mysql -u ekdac -pekdac
	* Password may be given immediately after -p (without any space in between)

```SQL
SHOW DATABASES;

-- set active database
USE kdacdb;

-- print current user and current database
SELECT USER(), DATABASE();

-- show db tables
SHOW TABLES;

EXIT;
```

* cmd> mysql -u ekdac -pekdac kdacdb
	* Login ekdac@localhost user with 'ekdac' password and set active database as kdacdb

```SQL
-- print current user and current database
SELECT USER(), DATABASE();

SELECT USER(), DATABASE() \g

SELECT USER(), DATABASE() \G

\! cls

SHOW TABLES;

CREATE TABLE students(id INT, name VARCHAR(20), marks DOUBLE);

SHOW TABLES;

INSERT INTO students VALUES (1, 'Nitin', 98.00);

INSERT INTO students VALUES (2, 'Prashant', 99.00);

INSERT INTO students VALUES (3, 'Nilesh', 77.00), (4, 'Sandeep', 88.00), (5, 'Amit', 90.00);

SELECT * FROM students;

```

# MySQL Db Physical Layout

* Db is stored in its data directory.
	* C:\ProgramData\MySQL\MySQL Server 8.0\Data

# Importing SQL file

* SOURCE command

```SQL
USE kdacdb;

SOURCE D:\pgdiploma\ekdac-dbt\data\classwork-db.sql

SHOW TABLES;

SELECT * FROM books;
```

* Create new database/schema for each SQL file.

```SQL
-- must be done using "root" login.
CREATE DATABASE hr;

GRANT ALL PRIVILEGES ON hr.* TO ekdac@localhost;

USE hr;

SOURCE D:\pgdiploma\ekdac-dbt\data\hr-db.sql
```


