# SQL
* Login with "ekdac" into mysql.
	* cmd> mysql -u ekdac -pekdac kdacdb

```SQL
SELECT USER(), DATABASE();

SOURCE D:/pgdiploma/ekdac-dbt/data/classwork-db.sql

SHOW TABLES;
```

## DDL - CREATE TABLE

* Datatypes of all RDBMS are similar (but not same).
	* MySQL: INT, DOUBLE, DECIMAL
	* Oracle: NUMBER
	* Derby: INTGER

```SQL
-- CREATE TABLE tablename(colname COL-TYPE, colname COL-TYPE, colname COL-TYPE, ...);
-- CREATE TABLE tablename(colname COL-TYPE constraint, colname COL-TYPE constraint, colname COL-TYPE constraint, ..., constraints);

CREATE TABLE test(c1 CHAR(10), c2 VARCHAR(10), c3 TEXT(10));

DESCRIBE test;

INSERT INTO test VALUES('ABCD', 'ABCD', 'ABCD');

SELECT * FROM test;

INSERT INTO test VALUES('abcdefghijk', 'abcdefghijk', 'abcdefghijk');
-- error: data too long for column c1

INSERT INTO test VALUES('abcdefghij', 'abcdefghijk', 'abcdefghijk');
-- error: data too long for column c2

INSERT INTO test VALUES('abcdefghij', 'abcdefghij', 'abcdefghijk');
-- allowed: c3 -- tinytext -- upto 255 characters

```

## DML - INSERT
	* INSERT INTO tablename VALUES (v1, v2, ...);
	* INSERT INTO tablename (colname1, colname2, ...) VALUES (v1, v2, ...);

```SQL
SELECT USER(), DATABASE();
-- ekdac user -- kdac db

SHOW TABLES;

SELECT * FROM students;

INSERT INTO students VALUES (6, 'shubham', 80);

INSERT INTO students (marks, id, name) VALUES (90, 7, 'samruddhi');

SELECT * FROM students;

INSERT INTO students (id, name) VALUES (8, 'akash');
-- insert new record: id=8, name='akash' and marks=NULL

SELECT * FROM students;

INSERT INTO students (id, name) VALUES (9, 'sameer'), (10, 'shekhar'), (11, 'rahul');

SELECT * FROM students;

INSERT INTO students VALUES (12, NULL, NULL);

```

* Can insert records from one table into anothers.
	* INSERT INTO newtablename SELECT * FROM oldtablename;
		* The number of columns and order columns in oldtable and newtable is same.
	* INSERT INTO newtablename(c1,c2) SELECT c1,c2 FROM oldtablename;
		* oldtable c1 and c2 will be inserted into newtable c1 and c2 respectively.

```SQL
CREATE TABLE newstudents (roll INT, name CHAR(20));

INSERT INTO newstudents SELECT * FROM students;
-- error

INSERT INTO newstudents(roll, name) SELECT id, name FROM students;

SELECT * FROM newstudents;
```

## DQL - SELECT

```SQL
DESCRIBE emp;

SELECT * FROM emp;
-- fetch all columns of the table

SELECT empno,ename,sal FROM emp;

```








