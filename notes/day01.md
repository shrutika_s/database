# Getting Started

## With root (admin) login

```SQL
CREATE DATABASE kdacdb;

CREATE USER ekdac@'localhost' IDENTIFIED BY 'ekdac';

GRANT ALL PRIVILEGES ON kdacdb.* TO ekdac@'localhost';

FLUSH PRIVILEGES;

EXIT;
```

## Set PATH variable for MySQL

* This PC --> Right click --> Properties --> Advanced System Settings --> Advanced --> Environment Variables
	* PATH -- Edit --> New --> C:\Program Files\MySQL\MySQL Server 8.0\bin --> OK --> OK --> OK

## With 'ekdac' user login.

1. Open command prompt/terminal.
2. cmd> mysql -u ekdac -p

```SQL
SHOW DATABASES;

EXIT;
```

